﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SocketCommunication
{
    class ServerProgram
    {
        static void Main(string[] args)
        {
           ServerExecution serverOperation = new ServerExecution();
           serverOperation.startServer();
           serverOperation.accept();    
           serverOperation.recieveRequestAndProcess();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
           serverOperation.sendResponse("");
           serverOperation.stopServer();
        }
    }

    public class ServerExecution
        {
            public Socket clientSocket;
            static IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName()); 
            static IPAddress address = host.AddressList[0]; 
            IPEndPoint sampleEndPoint = new IPEndPoint(address, 3020);
            Socket listener = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            public void accept(){
                clientSocket = listener.Accept();
                Console.WriteLine("Waiting for connection from client:");
            }

            public void recieveRequestAndProcess(){
                 Console.WriteLine("1");    
                byte[] bytes = new Byte[1024]; 
                Console.WriteLine("2");    
                string data = null; 
                Console.WriteLine("3");    
                while (true) { 
                    Console.WriteLine("4");    
                    int numByte = clientSocket.Receive(bytes); 
                    Console.WriteLine("5");    
                    data += Encoding.ASCII.GetString(bytes, 0, numByte); 
                    Console.WriteLine("6");      
                    Console.WriteLine("hoho", data);     
                     if (data.IndexOf("<EOF>") > -1) 
                     break; 
                     Console.WriteLine("7");    
                } 
                Console.WriteLine("Msg received from client -> {0} ", data); 
            }

            public void sendResponse(string message){
                byte[] testMessage = Encoding.ASCII.GetBytes(message);
                clientSocket.Send(testMessage);
            }

            public void startServer(){
                listener.Bind(sampleEndPoint);
                listener.Listen(3);
                Console.WriteLine("Server started!");
            }

            public void stopServer(){
                clientSocket.Shutdown(SocketShutdown.Both); 
                clientSocket.Close();
            }
    }

}
