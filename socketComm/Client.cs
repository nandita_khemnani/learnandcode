﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SocketCommunication
{
    public class ClientProgram
    { 
        static void Main(string[] args)
        {
            ClientExecution clientOperation = new ClientExecution();

            clientOperation.connect();
            clientOperation.sendMessage();
            clientOperation.recieveResponse();
            clientOperation.disconnect();
        }
    } 

    public class ClientExecution
    {
            static IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName()); 
            static IPAddress address = host.AddressList[0]; 
            IPEndPoint sampleEndPoint = new IPEndPoint(address, 3020);
            Socket sender = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp); 

        public void connect() {
            sender.Connect(sampleEndPoint); 
            Console.WriteLine("Connection established at port -> {0} ", sender.RemoteEndPoint.ToString()); 
        }

        public void sendMessage(){
            string message; 
            message = Console.ReadLine();
            Console.WriteLine("Message Sent to the Server. Msg-> '{0}'", message);
        }

        public void recieveResponse(){
            byte[] recievedMessage = new byte[1024];
            int recievedBytes = sender.Receive(recievedMessage); 
            Console.WriteLine("Message from Server -> {0}",Encoding.ASCII.GetString(recievedMessage, 0, recievedBytes)); 
        }

        public void disconnect(){
            sender.Shutdown(SocketShutdown.Both); 
            sender.Close();
        }
    } 


} 