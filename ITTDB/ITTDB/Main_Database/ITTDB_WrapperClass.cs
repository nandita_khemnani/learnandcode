using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ITTDB.Main_Database;
using System.IO;

namespace ITTDB.Main_Database
{
    public class ITTDB_WrapperClass
    {
        FileService operation = new FileService();
        //Serializing the list
        public void serialization(string fileName, List<object> list)

        {
            string serializedData = JsonConvert.SerializeObject(list);
            operation.writingListIntoFile(fileName, serializedData);
            
        }
        
        //Deserializing data into the file
        public string deserialization(string fileName)
        {
            string deserializingData;
            FileStream fileStream = File.OpenRead(fileName);
            using(StreamReader streamReader = new StreamReader(fileStream))
            {
                deserializingData = streamReader.ReadLine();
                fileStream.Close();
            }
            return deserializingData;
        }
    }
}