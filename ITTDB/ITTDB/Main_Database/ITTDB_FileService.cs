using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace ITTDB.Main_Database
{
    public class FileService
    {
        //setting ID of a object
        public void settingIdOfNewObject(List<object> listOfObject, object newObjToBeStored)
        {
            PropertyInfo objectProperty = newObjToBeStored.GetType().GetProperty("id");
            if(listOfObject.Count==0)
            {
                objectProperty.SetValue(newObjToBeStored, 1, null);
            }
            else
            {
                Object lastObj = listOfObject[listOfObject.Count - 1];
                JObject jobject = JObject.FromObject(lastObj);
                int id = (int)jobject["id"];
                objectProperty.SetValue(newObjToBeStored, id + 1, null);
            }
        }

        //Appending list with a new object
        public List<object> appendingTheListWithNewObject(List<object> listOfObject, Object newObjToBeStored)
        {
            listOfObject.Add(newObjToBeStored);
            return listOfObject;
        }

        //Writing the List into the file (Updated-list)
        public void writingListIntoFile(string fileName, string serializedData)
        {
            using (StreamWriter streamWriter = File.CreateText(fileName))
            {
                streamWriter.WriteLine(serializedData);
                streamWriter.Close();
            }
        }
 
        //Reading list of Json Object
        public string readingListOfJsonObject(string fileName)
        {
            return System.IO.File.ReadAllText(fileName);
        }

        //Deserializing the JsonString into an Object
        public List<object> deserializingJsonStringIntoAnObject(string deserializedData)
        {
            return JsonConvert.DeserializeObject<List<Object>>(deserializedData);
        }
    }
}