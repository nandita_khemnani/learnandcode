using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using ITTDB.Main_Database;

namespace ITTDB.Main_Database
{
    //class ITTDataBase contains all the CRUD operations performed on the Student class
    public class ITTDataBase : IhasITTDatabase
    {
        string filename;
        ITTDB_WrapperClass readerwriter = new ITTDB_WrapperClass();
        ITTDB_OperationsOnObject opr = new ITTDB_OperationsOnObject();

        List<Object> list = new List<Object>();

        //saving object(Student) into the file
        public void saveStudent(Object obj)
        {
            try
            {
                opr.isObjectNull(obj);
                filename = obj.GetType().Name;
                if (File.Exists(filename))
                {
                    opr.checkForFileLength(obj, filename);
                }
                else
                {
                    opr.setIdAndAppendListWithNewObject(obj);
                }
                readerwriter.serialization(filename, list);
            }
            catch (Exception exception)
            {
                throw new ITTDatabaseException("OBJECT-SAVE_ERROR", exception);
            }
        }

        //Finding the object(Student) in the file
        public object findStudent(object obj, string key, string expectedValue)
        {
            try
            {
                opr.isObjectNull(obj);
                filename = obj.GetType().Name;
                if (File.Exists(filename))
                {
                    if (new FileInfo(filename).Length != 0)
                    {
                        opr.deserializeListToGetFileLength(filename);
                        opr.returnObjectInTheList(expectedValue, key);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ITTDatabaseException("OBJECT-FIND_ERROR", exception);
            }
            return default(object);
        }

        //Deleting an object(Student) from the file
        public bool deleteStudent(object obj, int id)
        {
            try
            {
                opr.isObjectNull(obj);
                filename = obj.GetType().Name;
                if (File.Exists(filename))
                {
                    if (new FileInfo(filename).Length != 0)
                    {
                        opr.deserializeListToGetFileLength(filename);
                        foreach (var objInList in list)
                        {
                            opr.removingObjectIfFoundInTheList(objInList, id, filename);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ITTDatabaseException("OBJECT-DELETE_ERROR", exception);
            }
            return true;
        }

        //Edit an object into the file
        public bool editStudent(Object obj)
        {
            int indexOfList = 0;
            try
            {
                filename = obj.GetType().Name;
                if (File.Exists(filename))
                {
                    if (new FileInfo(filename).Length != 0)
                    {
                        //new way
                        JObject jsonObj = JObject.FromObject(obj);
                        int id = (int)jsonObj["id"];
                        opr.deserializeListToGetFileLength(filename);
                        foreach (var objInList in list)
                        {
                            indexOfList++;
                            JObject jsonObject = JObject.FromObject(objInList);
                            int idListObj = (int)jsonObj["id"];

                            if (id == idListObj)
                            {
                                list.Remove(objInList);
                                list.Insert(indexOfList - 1, obj);
                                readerwriter.serialization(filename, list);
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new ITTDatabaseException("OBJECT-EDIT_ERROR", exception);
            }
            return true;
        }  
    }
}
