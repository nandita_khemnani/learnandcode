using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ITTDB.Main_Database;
using System.IO;

namespace ITTDB.Main_Database
{
    public class ITTDB_OperationsOnObject
    {
        FileService operation = new FileService();
        ITTDB_WrapperClass readerwriter = new ITTDB_WrapperClass();

        List<Object> list = new List<Object>();
        /*
        **Functions used in Refactoring the code
        */
        //This function checks if the object is null and then throws the exception
        public bool isObjectNull(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("StoringObject", "object should not be null");
            }
            
            return true;
        }

        //This function returns an object if the actual object and object that is searched is same
        public object returnObjectInTheList(string expectedValue, string key)
        {
            foreach (var objInList in list)
            {
                JObject jsonObj = JObject.FromObject(objInList);
                string actualValue = (string)jsonObj[key];
                if (actualValue == expectedValue)
                {
                    return objInList;
                }
            }
             return null;
        }

        //This function checks if the file information is valid
        //If valid then append it into the list
        public void isFileInfoValid(object obj, string filename)
        {
            string deserializedData = readerwriter.deserialization(filename);
            list = operation.deserializingJsonStringIntoAnObject(deserializedData);

            operation.settingIdOfNewObject(list, obj);
            JObject jObject = JObject.FromObject(obj);

            int id = (int)jObject["id"];
            list = operation.appendingTheListWithNewObject(list, obj);
        }

        //This function sets id for a new object and append it into the list
        public void setIdAndAppendListWithNewObject(object obj)

        {
            operation.settingIdOfNewObject(list, obj);
            list = operation.appendingTheListWithNewObject(list, obj);
        }

        //This function checks the length of the file.
        //If the length is 0, it sets id for new object and appends it into the list
        public void checkForFileLength(object obj, string filename)
        {
            if (new FileInfo(filename).Length == 0)
            {
                operation.settingIdOfNewObject(list, obj);
                list = operation.appendingTheListWithNewObject(list, obj);
            }
            else
            {
                isFileInfoValid(obj,filename);
            }
        }

        //This function removes the object and serializes the list if the id  is matched with the searched id
        public bool removingObjectIfFoundInTheList(object objInList, int id, string filename)
        {
            JObject jsonObj = JObject.FromObject(objInList);
            int idListObj = (int)jsonObj["id"];
            if (id == idListObj)
            {
                list.Remove(objInList);
                readerwriter.serialization(filename, list);
                return true;
            }
            return false;
        }

        //This function deserialize file to obtain its length
        public void deserializeListToGetFileLength(string filename)
        {
            string deserializedData = readerwriter.deserialization(filename);
            List<Object> list = operation.deserializingJsonStringIntoAnObject(deserializedData);
        }
    }
}