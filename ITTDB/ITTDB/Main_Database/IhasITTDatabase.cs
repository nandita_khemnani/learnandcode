using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDB.Main_Database
{
    public interface IhasITTDatabase
    {
        void saveStudent(object obj);
        object findStudent(object obj, string key, string expectedValue);
        bool deleteStudent(object obj, int id);
        bool editStudent(object obj);
    }
}