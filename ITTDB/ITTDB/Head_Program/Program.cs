using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITTDB.Classes;
using ITTDB.Main_Database;

namespace ITTDB.Head_Program
{
    class Program
    {
        static void Main(string[] args){

            Student studentOne = new Student();
            studentOne.name="Nandita";
            studentOne.subject="ScienceMaths";

            Student studentTwo = new Student();
            studentTwo.name="Abhilasha";
            studentTwo.subject="CommerceMaths";

            //Add Student
            ITTDataBase db = new ITTDataBase();
            db.saveStudent(studentOne);
            db.saveStudent(studentTwo);

            //Find Student
            Object studentObject = db.findStudent(studentOne, "subject", "ScienceMaths");
            if(studentObject!=null)
            {
                Console.WriteLine(studentObject);
            }
            else{
                Console.WriteLine("Student Not found");
            }
            Console.Read();

            //Delete Student
            bool isStudentDeleted = db.deleteStudent(studentTwo,2);
            if(isStudentDeleted)
            {
                Console.WriteLine("Student deleted");
            }
            else{
                Console.WriteLine("Student Not found");
            }
            Console.Read();

            //Edit Student
            studentOne.setSubject("newSubject");
            bool isStudentEditable = db.editStudent(studentOne);
            if(isStudentEditable)
            {
                Console.WriteLine("Record modified");
            }
            else{
                Console.WriteLine("Student Not found");
            }
            Console.Read();

        }
}
}