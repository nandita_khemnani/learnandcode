using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITTDB.Classes
{
    [Serializable]
    public class Student
    {
       public int id;
       public string name;
       public string subject;

       public void setSubject(string newSubject)
        {
            this.subject = newSubject;
        }
    }
}
