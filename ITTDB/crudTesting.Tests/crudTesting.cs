using System;
using ITTDB.Main_Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITTDB.Classes;

namespace crudTesting.Tests
{
    [TestClass]
    public class UnitTest1
    {
        /*  [TestMethod]
         public void findStudentByName()
         {
            // Arrange
            ITTDataBase ittdb = new ITTDataBase();
            Student student = new Student();
            student.name = "Nandita";
            // Act
            Object expectedStudentObj = ittdb.findStudent(student.name, "name", "Nandita");
            // Assert
            Assert.AreEqual(expectedStudentObj, student.name);

         }  */

         /*  [TestMethod]
         public void findStudentById()
         {
            // Arrange
            ITTDataBase ittdb = new ITTDataBase();
            Student student = new Student();
            student.id = 1;
            // Act
            Object expectedStudentObj = ittdb.findStudent(student.id, "id", 1);
            // Assert
            Assert.AreEqual(expectedStudentObj, student.id);

         }  */
 
         [TestMethod]
         public void deleteStudent()
         {
            // Arrange
            Boolean expResult = true;
            ITTDataBase ittdb = new ITTDataBase();
            Student student = new Student();
            // Act
            Boolean actResult = ittdb.deleteStudent(student, 1);
            // Assert
            Assert.AreEqual(expResult, actResult);

         } 

         [TestMethod]
         public void editStudent()
         {
            // Arrange
            Boolean expResult = true;
            ITTDataBase ittdb = new ITTDataBase();
            Student student = new Student();
            student.id = 1;
            student.name = "Abhi";
            student.subject = "Arts";
            // Act
            Boolean actResult = ittdb.editStudent(student);
            // Assert
            Assert.AreEqual(expResult, actResult);

         } 
    }
}