// Spider_Problem1.cpp : Defines the entry point for the console application.


#include <list>
#include <iostream>
using namespace std;

class SpiderNode
{
public:
	int position;
	int power;

	SpiderNode()
	{
	}

	SpiderNode(int position, int power)
	{
		this->position = position;
		this->power = power;
	}
};

int power, numOfSpiders;
SpiderNode *spiderNode;
list<SpiderNode> spiderQueue;

void storePowerAndPosition()
{
	for (int index = 1; index <= numOfSpiders; index++)
	{
		cin >> power;
		spiderNode = new SpiderNode(index, power);
		spiderQueue.push_back(*spiderNode);
	}
}

/*Here, the main function takes a linked list is created with one node for each spider.
The node contains position & power of the spider.
Total no. of nodes is equal to the no. of spiders*/
int main()
{
	int selectedSpiders, maxPower, maxPowerPosition;

	//Enter the number of Spiders
	cin >> numOfSpiders;

	//Enter the number of Spiders to be selected at once
	cin >> selectedSpiders;
	
	//Function to store power and position of each Spider in the queue
	storePowerAndPosition();
	

	/*Loop that dequeues the selected spiders from the list.
	It finds the position of spider with max power and displays it.
	Then it eliminates the spider from the list*/ 
	for (int index = 0; index < selectedSpiders; index++)
	{
		int dequeueSpiders = selectedSpiders;
		int queueSize = spiderQueue.size();

		if (queueSize < selectedSpiders)
		{
			dequeueSpiders = queueSize;
		}
		
		SpiderNode currentSpider;
		maxPower = -1;
		list<SpiderNode>::iterator spiderPointer;
		spiderPointer = spiderQueue.begin();

		for (int position = 1; position <= dequeueSpiders; spiderPointer++, position++)
		{
			currentSpider = *spiderPointer;
			if (currentSpider.power > maxPower)
			{
				maxPower = currentSpider.power;
				maxPowerPosition = currentSpider.position;
			}
		}
		if (index == selectedSpiders - 1)
		{
			cout << maxPowerPosition;
			break;
		}
		else
		{
			cout << maxPowerPosition << " ";
		}

		int count = 0;
		for (int position = 1; position <= dequeueSpiders; position++)
		{
			currentSpider = spiderQueue.front();
			spiderQueue.pop_front();

			if (currentSpider.power == maxPower && count == 0)
			{
				count++;
			}
			else
			{
				if (currentSpider.power > 0)
				{
					currentSpider.power--;
				}

				spiderQueue.push_back(currentSpider);
			}
		}
	}
}
