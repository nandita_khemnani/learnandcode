using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Server.DatabaseOperations
{
    public class FileService
    {
        //store output files at this path
        public string getOutputFilePath(string fileName) {
            return fileName;
        }
        
        //setting ID of a object
        public object settingIdOfNewObject(List<object> listOfObject, object newObjToBeStored)
        {
            JObject jObject = JObject.FromObject(newObjToBeStored);
            if (listOfObject.Count == 0)
            {
                jObject["id"] = 1;
            }
            else
            {
                Object lastObject = listOfObject[listOfObject.Count - 1];
                JObject listJObject = JObject.FromObject(lastObject);
                int id = (int)listJObject["id"];
                jObject["id"] = id+1;
            }
            return jObject;
        }

        //Appending list with a new object
        public List<object> appendingTheListWithNewObject(List<object> listOfObject, Object newObjToBeStored)
        {
            listOfObject.Add(newObjToBeStored);
            return listOfObject;
        }

        //Writing the List into the file (Updated-list)
        public void writingListIntoFile(string fileName, string serializedData)
        {
            using (StreamWriter streamWriter = File.CreateText(fileName))
            {
                streamWriter.WriteLine(serializedData);
                streamWriter.Close();
            }
        }
        
        //Removing the object from the list
        public List<object> removingTheObjectFromObjectList(List<object> listOfObject, object objectToBeRemoved) {
            listOfObject.Remove(objectToBeRemoved);
            return listOfObject;

        }
    }
}