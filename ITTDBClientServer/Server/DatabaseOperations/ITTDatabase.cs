using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json.Linq;
using WrapperClass;
using Server.ServerProgram;

namespace Server.DatabaseOperations
{
    //class ITTDataBase contains all the CRUD operations performed on the Student class
    public class ITTDataBase
    {
        DBWrapperClass readerwriter = new DBWrapperClass();
        ResponseFromServer responseFromServer = new ResponseFromServer();
        ITTDB_OperationsOnObject opr = new ITTDB_OperationsOnObject();
        FileService fs = new FileService();
        List<Object> list = new List<Object>();
        string response;
        //string status;

        //saving object(Student) into the file
        public string saveStudent(string objectType, Object obj)
        {
            List<Object> list = new List<Object>();
            string filePath = fs.getOutputFilePath(objectType);
            try
            {
                opr.isObjectNull(obj);
                if (File.Exists(filePath))
                {
                    list =  readerwriter.deserialization(filePath);
                }
                object objj = fs.settingIdOfNewObject(list, obj);
                list = fs.appendingTheListWithNewObject(list, objj);
                string serializedData = readerwriter.serialization(list);
                fs.writingListIntoFile(filePath, serializedData);
                return responseFromServer.responseOnSuccess(objj, "Success", "201");
            }
            catch (Exception exception)
            {
                response = responseFromServer.responseOnFailure("Fail", "Couldn't save data", "Error code: Data not saved");
                throw new ITTDatabaseException("OBJECT-SAVE_ERROR", exception);
            }
        }

        //Finding the object(Student) in the file
        public string findStudent(string objectType, Object obj)
        {
            
            string filePath = fs.getOutputFilePath(objectType);
            try
            {
                if (File.Exists(filePath))
                {
                    if (new FileInfo(filePath).Length != 0)
                    {
                        List<Object> list = readerwriter.deserialization(filePath);
                        Object objectReturned = opr.returnObjectInTheList(obj, list);
                        return responseFromServer.responseOnSuccess(objectReturned, "Found", "200");
                    }
                }
            }
            catch (Exception exception)
            {
                response = responseFromServer.responseOnFailure("Fail", "Couldn't find data", "Error code: Data not found");
                throw new ITTDatabaseException("OBJECT-FIND_ERROR", exception);
            }
            return null;
        }

        //Deleting an object(Student) from the file
        public string deleteStudent(string objectType, Object obj)
        {
            string filePath = fs.getOutputFilePath(objectType);
            try
            {
                if (File.Exists(filePath))
                {
                    if (new FileInfo(filePath).Length != 0)
                    {
                        List<Object> list = readerwriter.deserialization(filePath);
                        var objectDel = opr.removingObjectIfFoundInTheList(filePath, obj, list);
                        return responseFromServer.responseOnSuccess(objectDel, "Deleted", "200");
                    }
                }
            }
            catch (Exception exception)
            {
                response = responseFromServer.responseOnFailure("Fail", "Couldn't delete data", "Error code: Data not deleted");
                throw new ITTDatabaseException("OBJECT-DELETE_ERROR", exception);
            }
            return null;
        }

        //Edit an object into the file
        public string editStudent(string objectType, Object obj)
        {
            string filePath = fs.getOutputFilePath(objectType);
            int indexOfList = 0;
            try
            {
                if (File.Exists(filePath))
                {
                    if (new FileInfo(filePath).Length != 0)
                    {
                        list = readerwriter.deserialization(filePath);
                        JObject jsonObj = JObject.FromObject(obj);
                        int id = (int)jsonObj["id"];
                        foreach (var objInList in list)
                        {
                            indexOfList++;
                            JObject jsonObject = JObject.FromObject(objInList);
                            int idListObj = (int)jsonObj["id"];
                            if (id == idListObj)
                            {
                                list.Remove(objInList);
                                list.Insert(indexOfList - 1, obj);
                                string serializedData = readerwriter.serialization(list);
                                fs.writingListIntoFile(filePath, serializedData);
                                return responseFromServer.responseOnSuccess(jsonObj, "Updated", "200");
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                response = responseFromServer.responseOnFailure("Fail", "Couldn't edit data", "Error code: Data not modified");
                throw new ITTDatabaseException("OBJECT-EDIT_ERROR", exception);
            }
            return null;
        }  
    }
}
