using System;

namespace Server.DatabaseOperations
{
    //class ITTDatabaseException handles custom exceptions
    public class ITTDatabaseException : Exception
    {
         //Default Constructor
        public ITTDatabaseException()
            :base()
        { }
        //Parameterised Constructor
        public ITTDatabaseException(string errorCode, Exception exception)
        {
            switch (errorCode)
            {
                case "OBJECT-SAVE_ERROR":
                    Console.WriteLine("Object is not saved, see the details below {0}", exception.Message);
                    break;

                case "OBJECT-FIND_ERROR":
                    Console.WriteLine("Object is not found, see the details below {0}", exception.Message);
                    break;

                case "OBJECT-DELETE_ERROR":
                    Console.WriteLine("Object is not deleted, see the details below {0}", exception.Message);
                    break;

                case "OBJECT-EDIT_ERROR":
                    Console.WriteLine("Object is not editable in the db, see below {0}", exception.Message);
                    break;

            }
        }
    }
}