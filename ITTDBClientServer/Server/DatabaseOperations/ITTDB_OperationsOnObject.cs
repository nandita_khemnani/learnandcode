using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;
using WrapperClass;

namespace Server.DatabaseOperations
{
    public class ITTDB_OperationsOnObject
    {
        FileService operation = new FileService();
        DBWrapperClass readerwriter = new DBWrapperClass();

        List<Object> list = new List<Object>();
        /*
        **Functions used in Refactoring the code
        */
        //This function checks if the object is null and then throws the exception
        public bool isObjectNull(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("StoringObject", "object should not be null");
            }
            
            return true;
        }

        //This function returns an object if the actual object and object that is searched is same
        public object returnObjectInTheList(Object obj, List<object> list)
        {
            JObject jObject = JObject.FromObject(obj);
            string key = (string)jObject["key"];
            string expectedValue = (string)jObject["value"];
            foreach (var objInList in list)
            {
                JObject jsonObj = JObject.FromObject(objInList);
                string actualValue = (string)jsonObj[key];
                if (actualValue == expectedValue)
                {
                    return objInList;
                }
            }
             return null;
        }

        //This function checks if the file information is valid
        //If valid then append it into the list
        public List<object> isFileInfoValid(object obj, string objectType)
        {
            string filePath = operation.getOutputFilePath(objectType);
            List<Object> list = new List<Object>();
            list = readerwriter.deserialization(filePath);
            setIdAndAppendListWithNewObject(obj);
            Console.WriteLine("ye list h {0} ", list);
            return list;
        }

        //This function sets id for a new object and append it into the list
        public List<object> setIdAndAppendListWithNewObject(object obj)

        {
            Object objectt = operation.settingIdOfNewObject(list, obj);
            list = operation.appendingTheListWithNewObject(list, objectt);
            Console.WriteLine("3rd {0}", list);
            return list;
        }

        //This function checks the length of the file.
        //If the length is 0, it sets id for new object and appends it into the list
        public List<object> checkForFileLength(object obj, string filePath)
        {
            if (new FileInfo(filePath).Length == 0)
            {
                operation.settingIdOfNewObject(list, obj);
                list = operation.appendingTheListWithNewObject(list, obj);
            }
            else
            {
                Console.WriteLine("check for file length");
                list = isFileInfoValid(obj,filePath);
                
            }
            return list;
        }

        //This function removes the object and serializes the list if the id  is matched with the searched id
        public object removingObjectIfFoundInTheList(string filePath, Object obj, List<object> list)
        {
            Object listObject = null;
            JObject jsonObj = JObject.FromObject(obj);
            int idListObj = (int)jsonObj["id"];
            
            listObject = list.Find(x => (int)JObject.FromObject(x)["id"] == idListObj);
            if (listObject != null)
            {
                operation.removingTheObjectFromObjectList(list, listObject);
                string serializedData = readerwriter.serialization(list);
                operation.writingListIntoFile(filePath, serializedData);
                return listObject;
            }
            return null;
        }
    }
}