using Newtonsoft.Json.Linq;
using Server.DatabaseOperations;

namespace Server.ServerProgram
{
    public class ProcessClientRequest
    {
         ITTDataBase db = new ITTDataBase();
        public string processRequest(string processedData){
           ITTDataBase db = new ITTDataBase();
            string response;
            object obj = JObject.Parse(processedData);
            JObject jObject = JObject.FromObject(obj);
            string operationOnObject = (string)jObject["Operation"];
            string dataType = (string)jObject["Type"];
            object data = (object)jObject["Data"];
            switch(operationOnObject)
             {
                case "Save":    
                    response = db.saveStudent(dataType, data);
                    break;
                case "Find":    
                    response = db.findStudent(dataType, data);
                    break;
                case "Delete":    
                    response = db.deleteStudent(dataType, data);
                    break; 
                case "Update":    
                    response = db.editStudent(dataType, data);
                    break;
                default:
                    response = "Invalid Operation";
                    break; 
             }
             return response;
        }
    }
}