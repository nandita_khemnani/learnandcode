using System;
using System.Net;
using System.Text;
using System.Net.Sockets;

namespace Server.ServerProgram
{
    public class OperationsOnServer
    {
        static IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName()); 
        static IPAddress address = host.AddressList[0]; 
        IPEndPoint sampleEndPoint = new IPEndPoint(address, 3021);
        Socket listener = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);         

        public Socket startServer(){
            listener.Bind(sampleEndPoint);
            listener.Listen(3);
            Console.WriteLine("Server started!");
            return listener;
        }
        public Socket accept(Socket listener){
            return listener.Accept();
        }
        public string receiveRequest(Socket socket)
        {

            byte[] bytes = new Byte[socket.ReceiveBufferSize];

            int byteReceived = socket.Receive(bytes);
            string data = Encoding.ASCII.GetString(bytes, 0, byteReceived);

            return data; 
        }
        public void sendResponse(Socket socket, byte[] message)
        {
            socket.Send(message);
        }
    }
}