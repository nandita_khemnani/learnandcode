using WrapperClass;

namespace Server.ServerProgram
{
    public class ResponseFromServer{

        DBWrapperClass readerwriter = new DBWrapperClass();
        public string responseOnSuccess(object data, string status, string code){
            var successResponse = new {
                 Data = data,
                 Status = status,
                 Code = code
             };

            string responseDataToBeSerialized = readerwriter.objectSerialization(successResponse);
            return responseDataToBeSerialized;
        }
        public string responseOnFailure(string status, string errorMessage, string errorCode){
            var successResponse = new {
                 Status = status,
                 ErrorMessage = errorMessage,
                 ErrorCode = errorCode
             };

            string responseDataToBeSerialized = readerwriter.objectSerialization(successResponse);
            return responseDataToBeSerialized;
        }
    }
    
}