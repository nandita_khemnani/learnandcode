using System;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server.ServerProgram
{
    public class ServerExecution
    {
        OperationsOnServer oprOnServer = new OperationsOnServer();
        ProcessClientRequest clientProcessor = new ProcessClientRequest();

        public void executeServer(){
            Socket listener = oprOnServer.startServer();
            int i= 1;
            while (true)
            {
                Socket clientSock = oprOnServer.accept(listener);
                Console.WriteLine("Connected to client {0}", i++);
                //executeClientRequest(clientSock);
                Task task = new Task(executeClientRequest, clientSock);
                task.Start();
            }
            
        }
        public void executeClientRequest(object clientSocket){
            Socket socket = (Socket)clientSocket;
            string dataRecievedFromClient = oprOnServer.receiveRequest(socket);
            Console.WriteLine("Request recieved from client side {0}", dataRecievedFromClient);
            Console.ReadLine();
            string response = clientProcessor.processRequest(dataRecievedFromClient);
            byte[] message = Encoding.ASCII.GetBytes(response);
            oprOnServer.sendResponse(socket, message);
            Console.WriteLine("Response Sent");

        }
    }
}