﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;

namespace WrapperClass
{
    public class DBWrapperClass
    {
        //Serializing the list
        public string serialization(List<object> list)
        {
            string serializedData = JsonConvert.SerializeObject(list);
            return serializedData; 
        }

        public string objectSerialization(Object obj)
        {
            string serializedData = JsonConvert.SerializeObject(obj);
            return serializedData;
        }
        
        //Deserializing data into the file
        public List<Object> deserialization(string fileName)
        {
            string deserializingData;
            FileStream fileStream = File.OpenRead(fileName);
            using(StreamReader streamReader = new StreamReader(fileStream))
            {
                deserializingData = streamReader.ReadLine();
                fileStream.Close();
            }
            return JsonConvert.DeserializeObject<List<Object>>(deserializingData);
        }
    }
}
