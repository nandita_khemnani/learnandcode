using System.Text;

namespace CLient.ClientProgram
{
    public class RequestFromCLient{

        ExecutionFromClient clientex = new ExecutionFromClient();

        WrapperClass.DBWrapperClass wrapperClass = new WrapperClass.DBWrapperClass();
        //DBWrapperClass wrapperClass = new DBWrapperClass();
        public void generateRequestFromClient(string opertationPerformed, string typeOfData, object data){
             var obj = new {
                 Operation = opertationPerformed,
                 Type = typeOfData,
                 Data = data
             };
            string dataToBeSerialized = wrapperClass.objectSerialization(obj);
            byte[] requestData = Encoding.ASCII.GetBytes(dataToBeSerialized);
            clientex.executeClient(requestData);
    }
         
    }
}