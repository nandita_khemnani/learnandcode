using System;
using System.Net.Sockets;
using System.Text;
using System.Net;

namespace CLient.ClientProgram
{
    public class OperationOnClient
    {
        static IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName()); 
        static IPAddress address = host.AddressList[0]; 
        Socket sender;

        public void InitializeSender()
        {
            sender = new Socket(address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        } 

        public Socket connect() 
        {
            this.InitializeSender();
            IPEndPoint sampleEndPoint = new IPEndPoint(address, 3021);
            sender.Connect(sampleEndPoint); 
            Console.WriteLine("Connection established at port -> {0} ", sender.RemoteEndPoint.ToString());
            return sender;
        }
       
        public void sendResponse(Socket socket, byte[] message)
        {
            socket.Send(message);
        }
        
        public string receiveRequest(Socket socket)
        {

            byte[] bytes = new Byte[socket.ReceiveBufferSize];

            int byteReceived = socket.Receive(bytes);
            string data = Encoding.ASCII.GetString(bytes, 0, byteReceived);

            return data; 
        }
        
        public void disconnect(Socket socket)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}