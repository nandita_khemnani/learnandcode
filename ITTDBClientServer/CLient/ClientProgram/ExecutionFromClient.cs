using System;
using System.Net.Sockets;

namespace CLient.ClientProgram
{
    public class ExecutionFromClient
    {
        OperationOnClient operationOnClient = new OperationOnClient();

         public void executeClient(Byte[] dataRequested){
             Socket sender = operationOnClient.connect();

             operationOnClient.sendResponse(sender, dataRequested);
             Console.WriteLine("Data request sent to the server");

             string dataRecieved;
             dataRecieved = operationOnClient.receiveRequest(sender);
             Console.WriteLine("Data recieved from server->  {0}", dataRecieved);

             operationOnClient.disconnect(sender);
             Console.WriteLine("Connection closed");  
         }
    }
}